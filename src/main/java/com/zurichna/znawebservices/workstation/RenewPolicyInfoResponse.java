
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RenewPolicyInfoResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RenewPolicyInfoResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}ResponseBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="policy" type="{http://workstation.znawebservices.zurichna.com}ArrayOfPolicys" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RenewPolicyInfoResponse", propOrder = {
    "policy"
})
public class RenewPolicyInfoResponse
    extends ResponseBase
{

    protected ArrayOfPolicys policy;

    /**
     * Gets the value of the policy property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfPolicys }
     *     
     */
    public ArrayOfPolicys getPolicy() {
        return policy;
    }

    /**
     * Sets the value of the policy property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfPolicys }
     *     
     */
    public void setPolicy(ArrayOfPolicys value) {
        this.policy = value;
    }

}
