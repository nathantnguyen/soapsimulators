
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RetrieveUserHistoryResult" type="{http://workstation.znawebservices.zurichna.com}RetrieveUserHistoryResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "retrieveUserHistoryResult"
})
@XmlRootElement(name = "RetrieveUserHistoryResponse")
public class RetrieveUserHistoryResponse {

    @XmlElement(name = "RetrieveUserHistoryResult")
    protected RetrieveUserHistoryResponse2 retrieveUserHistoryResult;

    /**
     * Gets the value of the retrieveUserHistoryResult property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveUserHistoryResponse2 }
     *     
     */
    public RetrieveUserHistoryResponse2 getRetrieveUserHistoryResult() {
        return retrieveUserHistoryResult;
    }

    /**
     * Sets the value of the retrieveUserHistoryResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveUserHistoryResponse2 }
     *     
     */
    public void setRetrieveUserHistoryResult(RetrieveUserHistoryResponse2 value) {
        this.retrieveUserHistoryResult = value;
    }

}
