
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BindSubmissionRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BindSubmissionRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}RequestBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="masterPolicy" type="{http://workstation.znawebservices.zurichna.com}MasterPolicy" minOccurs="0"/&gt;
 *         &lt;element name="SMSN_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="changeState" type="{http://workstation.znawebservices.zurichna.com}ChangeFlag"/&gt;
 *         &lt;element name="submissionProduct" type="{http://workstation.znawebservices.zurichna.com}ArrayOfSubmissionBindProduct" minOccurs="0"/&gt;
 *         &lt;element name="policyBind" type="{http://workstation.znawebservices.zurichna.com}PolicyBind" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BindSubmissionRequest", propOrder = {
    "masterPolicy",
    "smsnid",
    "changeState",
    "submissionProduct",
    "policyBind"
})
public class BindSubmissionRequest
    extends RequestBase
{

    protected MasterPolicy masterPolicy;
    @XmlElement(name = "SMSN_ID")
    protected int smsnid;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ChangeFlag changeState;
    protected ArrayOfSubmissionBindProduct submissionProduct;
    protected PolicyBind policyBind;

    /**
     * Gets the value of the masterPolicy property.
     * 
     * @return
     *     possible object is
     *     {@link MasterPolicy }
     *     
     */
    public MasterPolicy getMasterPolicy() {
        return masterPolicy;
    }

    /**
     * Sets the value of the masterPolicy property.
     * 
     * @param value
     *     allowed object is
     *     {@link MasterPolicy }
     *     
     */
    public void setMasterPolicy(MasterPolicy value) {
        this.masterPolicy = value;
    }

    /**
     * Gets the value of the smsnid property.
     * 
     */
    public int getSMSNID() {
        return smsnid;
    }

    /**
     * Sets the value of the smsnid property.
     * 
     */
    public void setSMSNID(int value) {
        this.smsnid = value;
    }

    /**
     * Gets the value of the changeState property.
     * 
     * @return
     *     possible object is
     *     {@link ChangeFlag }
     *     
     */
    public ChangeFlag getChangeState() {
        return changeState;
    }

    /**
     * Sets the value of the changeState property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeFlag }
     *     
     */
    public void setChangeState(ChangeFlag value) {
        this.changeState = value;
    }

    /**
     * Gets the value of the submissionProduct property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSubmissionBindProduct }
     *     
     */
    public ArrayOfSubmissionBindProduct getSubmissionProduct() {
        return submissionProduct;
    }

    /**
     * Sets the value of the submissionProduct property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSubmissionBindProduct }
     *     
     */
    public void setSubmissionProduct(ArrayOfSubmissionBindProduct value) {
        this.submissionProduct = value;
    }

    /**
     * Gets the value of the policyBind property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyBind }
     *     
     */
    public PolicyBind getPolicyBind() {
        return policyBind;
    }

    /**
     * Sets the value of the policyBind property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyBind }
     *     
     */
    public void setPolicyBind(PolicyBind value) {
        this.policyBind = value;
    }

}
