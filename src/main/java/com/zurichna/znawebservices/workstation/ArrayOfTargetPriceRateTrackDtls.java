
package com.zurichna.znawebservices.workstation;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfTargetPriceRateTrackDtls complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfTargetPriceRateTrackDtls"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TargetPriceRateTrackDtls" type="{http://workstation.znawebservices.zurichna.com}TargetPriceRateTrackDtls" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfTargetPriceRateTrackDtls", propOrder = {
    "targetPriceRateTrackDtls"
})
public class ArrayOfTargetPriceRateTrackDtls {

    @XmlElement(name = "TargetPriceRateTrackDtls", nillable = true)
    protected List<TargetPriceRateTrackDtls> targetPriceRateTrackDtls;

    /**
     * Gets the value of the targetPriceRateTrackDtls property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the targetPriceRateTrackDtls property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTargetPriceRateTrackDtls().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TargetPriceRateTrackDtls }
     * 
     * 
     */
    public List<TargetPriceRateTrackDtls> getTargetPriceRateTrackDtls() {
        if (targetPriceRateTrackDtls == null) {
            targetPriceRateTrackDtls = new ArrayList<TargetPriceRateTrackDtls>();
        }
        return this.targetPriceRateTrackDtls;
    }

}
