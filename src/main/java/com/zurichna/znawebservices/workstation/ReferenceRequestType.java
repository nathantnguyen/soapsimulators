
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReferenceRequestType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ReferenceRequestType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="All"/&gt;
 *     &lt;enumeration value="Country"/&gt;
 *     &lt;enumeration value="Organization"/&gt;
 *     &lt;enumeration value="Producer"/&gt;
 *     &lt;enumeration value="State"/&gt;
 *     &lt;enumeration value="Employee"/&gt;
 *     &lt;enumeration value="Product"/&gt;
 *     &lt;enumeration value="SIC"/&gt;
 *     &lt;enumeration value="UnderwritingProgram"/&gt;
 *     &lt;enumeration value="GeoLocation"/&gt;
 *     &lt;enumeration value="Carrier"/&gt;
 *     &lt;enumeration value="ReasonCode"/&gt;
 *     &lt;enumeration value="ReInsuranceProgram"/&gt;
 *     &lt;enumeration value="CompetitorPrice"/&gt;
 *     &lt;enumeration value="BillType"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ReferenceRequestType")
@XmlEnum
public enum ReferenceRequestType {

    @XmlEnumValue("All")
    ALL("All"),
    @XmlEnumValue("Country")
    COUNTRY("Country"),
    @XmlEnumValue("Organization")
    ORGANIZATION("Organization"),
    @XmlEnumValue("Producer")
    PRODUCER("Producer"),
    @XmlEnumValue("State")
    STATE("State"),
    @XmlEnumValue("Employee")
    EMPLOYEE("Employee"),
    @XmlEnumValue("Product")
    PRODUCT("Product"),
    SIC("SIC"),
    @XmlEnumValue("UnderwritingProgram")
    UNDERWRITING_PROGRAM("UnderwritingProgram"),
    @XmlEnumValue("GeoLocation")
    GEO_LOCATION("GeoLocation"),
    @XmlEnumValue("Carrier")
    CARRIER("Carrier"),
    @XmlEnumValue("ReasonCode")
    REASON_CODE("ReasonCode"),
    @XmlEnumValue("ReInsuranceProgram")
    RE_INSURANCE_PROGRAM("ReInsuranceProgram"),
    @XmlEnumValue("CompetitorPrice")
    COMPETITOR_PRICE("CompetitorPrice"),
    @XmlEnumValue("BillType")
    BILL_TYPE("BillType");
    private final String value;

    ReferenceRequestType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ReferenceRequestType fromValue(String v) {
        for (ReferenceRequestType c: ReferenceRequestType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
