
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AutoIndicationPrcResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AutoIndicationPrcResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}ResponseBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="techPricResponse" type="{http://workstation.znawebservices.zurichna.com}AutoTechPricResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AutoIndicationPrcResponse", propOrder = {
    "techPricResponse"
})
public class AutoIndicationPrcResponse
    extends ResponseBase
{

    protected AutoTechPricResponse techPricResponse;

    /**
     * Gets the value of the techPricResponse property.
     * 
     * @return
     *     possible object is
     *     {@link AutoTechPricResponse }
     *     
     */
    public AutoTechPricResponse getTechPricResponse() {
        return techPricResponse;
    }

    /**
     * Sets the value of the techPricResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link AutoTechPricResponse }
     *     
     */
    public void setTechPricResponse(AutoTechPricResponse value) {
        this.techPricResponse = value;
    }

}
