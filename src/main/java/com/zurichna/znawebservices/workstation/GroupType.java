
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GroupType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="GroupType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="All"/&gt;
 *     &lt;enumeration value="Zurich"/&gt;
 *     &lt;enumeration value="Maryland"/&gt;
 *     &lt;enumeration value="FD"/&gt;
 *     &lt;enumeration value="Canada"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "GroupType")
@XmlEnum
public enum GroupType {

    @XmlEnumValue("All")
    ALL("All"),
    @XmlEnumValue("Zurich")
    ZURICH("Zurich"),
    @XmlEnumValue("Maryland")
    MARYLAND("Maryland"),
    FD("FD"),
    @XmlEnumValue("Canada")
    CANADA("Canada");
    private final String value;

    GroupType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GroupType fromValue(String v) {
        for (GroupType c: GroupType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
