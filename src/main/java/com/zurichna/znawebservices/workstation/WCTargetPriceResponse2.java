
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WCTargetPriceResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WCTargetPriceResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="wcTargetPricedata" type="{http://workstation.znawebservices.zurichna.com}WCTargetPriceResponseData" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WCTargetPriceResponse", propOrder = {
    "wcTargetPricedata"
})
public class WCTargetPriceResponse2 {

    protected WCTargetPriceResponseData wcTargetPricedata;

    /**
     * Gets the value of the wcTargetPricedata property.
     * 
     * @return
     *     possible object is
     *     {@link WCTargetPriceResponseData }
     *     
     */
    public WCTargetPriceResponseData getWcTargetPricedata() {
        return wcTargetPricedata;
    }

    /**
     * Sets the value of the wcTargetPricedata property.
     * 
     * @param value
     *     allowed object is
     *     {@link WCTargetPriceResponseData }
     *     
     */
    public void setWcTargetPricedata(WCTargetPriceResponseData value) {
        this.wcTargetPricedata = value;
    }

}
