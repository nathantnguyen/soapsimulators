
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetWCAHTargetPrcDtls complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetWCAHTargetPrcDtls"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}RequestBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="targetPrcRq" type="{http://workstation.znawebservices.zurichna.com}WCTargetPriceRequest" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetWCAHTargetPrcDtls", propOrder = {
    "targetPrcRq"
})
public class GetWCAHTargetPrcDtls
    extends RequestBase
{

    protected WCTargetPriceRequest targetPrcRq;

    /**
     * Gets the value of the targetPrcRq property.
     * 
     * @return
     *     possible object is
     *     {@link WCTargetPriceRequest }
     *     
     */
    public WCTargetPriceRequest getTargetPrcRq() {
        return targetPrcRq;
    }

    /**
     * Sets the value of the targetPrcRq property.
     * 
     * @param value
     *     allowed object is
     *     {@link WCTargetPriceRequest }
     *     
     */
    public void setTargetPrcRq(WCTargetPriceRequest value) {
        this.targetPrcRq = value;
    }

}
