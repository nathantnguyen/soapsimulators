
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MVRDataSubmissionProduct complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MVRDataSubmissionProduct"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CUSTM_INS_SPEC_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PRDT_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDT_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Driver" type="{http://workstation.znawebservices.zurichna.com}ArrayOfDriver" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MVRDataSubmissionProduct", propOrder = {
    "custminsspecid",
    "prdtnm",
    "prdtabbr",
    "driver"
})
public class MVRDataSubmissionProduct {

    @XmlElement(name = "CUSTM_INS_SPEC_ID")
    protected int custminsspecid;
    @XmlElement(name = "PRDT_NM")
    protected String prdtnm;
    @XmlElement(name = "PRDT_ABBR")
    protected String prdtabbr;
    @XmlElement(name = "Driver")
    protected ArrayOfDriver driver;

    /**
     * Gets the value of the custminsspecid property.
     * 
     */
    public int getCUSTMINSSPECID() {
        return custminsspecid;
    }

    /**
     * Sets the value of the custminsspecid property.
     * 
     */
    public void setCUSTMINSSPECID(int value) {
        this.custminsspecid = value;
    }

    /**
     * Gets the value of the prdtnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDTNM() {
        return prdtnm;
    }

    /**
     * Sets the value of the prdtnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDTNM(String value) {
        this.prdtnm = value;
    }

    /**
     * Gets the value of the prdtabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDTABBR() {
        return prdtabbr;
    }

    /**
     * Sets the value of the prdtabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDTABBR(String value) {
        this.prdtabbr = value;
    }

    /**
     * Gets the value of the driver property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfDriver }
     *     
     */
    public ArrayOfDriver getDriver() {
        return driver;
    }

    /**
     * Sets the value of the driver property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfDriver }
     *     
     */
    public void setDriver(ArrayOfDriver value) {
        this.driver = value;
    }

}
