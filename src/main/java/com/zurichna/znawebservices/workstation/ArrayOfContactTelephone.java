
package com.zurichna.znawebservices.workstation;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfContactTelephone complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfContactTelephone"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ContactTelephone" type="{http://workstation.znawebservices.zurichna.com}ContactTelephone" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfContactTelephone", propOrder = {
    "contactTelephone"
})
public class ArrayOfContactTelephone {

    @XmlElement(name = "ContactTelephone", nillable = true)
    protected List<ContactTelephone> contactTelephone;

    /**
     * Gets the value of the contactTelephone property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contactTelephone property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContactTelephone().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContactTelephone }
     * 
     * 
     */
    public List<ContactTelephone> getContactTelephone() {
        if (contactTelephone == null) {
            contactTelephone = new ArrayList<ContactTelephone>();
        }
        return this.contactTelephone;
    }

}
