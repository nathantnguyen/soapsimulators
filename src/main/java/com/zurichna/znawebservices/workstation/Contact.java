
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Contact complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Contact"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ROL_GRP_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ROL_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="GPNG_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ROL_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="LST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="PTY_ROL_REL_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PRR_ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="changeState" type="{http://workstation.znawebservices.zurichna.com}ChangeFlag"/&gt;
 *         &lt;element name="contactAddress" type="{http://workstation.znawebservices.zurichna.com}ArrayOfContactAddress" minOccurs="0"/&gt;
 *         &lt;element name="contactTelephone" type="{http://workstation.znawebservices.zurichna.com}ArrayOfContactTelephone" minOccurs="0"/&gt;
 *         &lt;element name="contactRole" type="{http://workstation.znawebservices.zurichna.com}ArrayOfContactRole" minOccurs="0"/&gt;
 *         &lt;element name="contactComment" type="{http://workstation.znawebservices.zurichna.com}ArrayOfContactComment" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Contact", propOrder = {
    "rolgrpnm",
    "rolnm",
    "gpngid",
    "rolid",
    "ptyid",
    "lstnm",
    "fstnm",
    "nm",
    "enttistmp",
    "ptyrolrelid",
    "prrenttistmp",
    "changeState",
    "contactAddress",
    "contactTelephone",
    "contactRole",
    "contactComment"
})
public class Contact {

    @XmlElement(name = "ROL_GRP_NM")
    protected String rolgrpnm;
    @XmlElement(name = "ROL_NM")
    protected String rolnm;
    @XmlElement(name = "GPNG_ID")
    protected int gpngid;
    @XmlElement(name = "ROL_ID")
    protected int rolid;
    @XmlElement(name = "PTY_ID")
    protected int ptyid;
    @XmlElement(name = "LST_NM")
    protected String lstnm;
    @XmlElement(name = "FST_NM")
    protected String fstnm;
    @XmlElement(name = "NM")
    protected String nm;
    @XmlElement(name = "ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar enttistmp;
    @XmlElement(name = "PTY_ROL_REL_ID")
    protected int ptyrolrelid;
    @XmlElement(name = "PRR_ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar prrenttistmp;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ChangeFlag changeState;
    protected ArrayOfContactAddress contactAddress;
    protected ArrayOfContactTelephone contactTelephone;
    protected ArrayOfContactRole contactRole;
    protected ArrayOfContactComment contactComment;

    /**
     * Gets the value of the rolgrpnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getROLGRPNM() {
        return rolgrpnm;
    }

    /**
     * Sets the value of the rolgrpnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setROLGRPNM(String value) {
        this.rolgrpnm = value;
    }

    /**
     * Gets the value of the rolnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getROLNM() {
        return rolnm;
    }

    /**
     * Sets the value of the rolnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setROLNM(String value) {
        this.rolnm = value;
    }

    /**
     * Gets the value of the gpngid property.
     * 
     */
    public int getGPNGID() {
        return gpngid;
    }

    /**
     * Sets the value of the gpngid property.
     * 
     */
    public void setGPNGID(int value) {
        this.gpngid = value;
    }

    /**
     * Gets the value of the rolid property.
     * 
     */
    public int getROLID() {
        return rolid;
    }

    /**
     * Sets the value of the rolid property.
     * 
     */
    public void setROLID(int value) {
        this.rolid = value;
    }

    /**
     * Gets the value of the ptyid property.
     * 
     */
    public int getPTYID() {
        return ptyid;
    }

    /**
     * Sets the value of the ptyid property.
     * 
     */
    public void setPTYID(int value) {
        this.ptyid = value;
    }

    /**
     * Gets the value of the lstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLSTNM() {
        return lstnm;
    }

    /**
     * Sets the value of the lstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLSTNM(String value) {
        this.lstnm = value;
    }

    /**
     * Gets the value of the fstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFSTNM() {
        return fstnm;
    }

    /**
     * Sets the value of the fstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFSTNM(String value) {
        this.fstnm = value;
    }

    /**
     * Gets the value of the nm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNM() {
        return nm;
    }

    /**
     * Sets the value of the nm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNM(String value) {
        this.nm = value;
    }

    /**
     * Gets the value of the enttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getENTTISTMP() {
        return enttistmp;
    }

    /**
     * Sets the value of the enttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setENTTISTMP(XMLGregorianCalendar value) {
        this.enttistmp = value;
    }

    /**
     * Gets the value of the ptyrolrelid property.
     * 
     */
    public int getPTYROLRELID() {
        return ptyrolrelid;
    }

    /**
     * Sets the value of the ptyrolrelid property.
     * 
     */
    public void setPTYROLRELID(int value) {
        this.ptyrolrelid = value;
    }

    /**
     * Gets the value of the prrenttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPRRENTTISTMP() {
        return prrenttistmp;
    }

    /**
     * Sets the value of the prrenttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPRRENTTISTMP(XMLGregorianCalendar value) {
        this.prrenttistmp = value;
    }

    /**
     * Gets the value of the changeState property.
     * 
     * @return
     *     possible object is
     *     {@link ChangeFlag }
     *     
     */
    public ChangeFlag getChangeState() {
        return changeState;
    }

    /**
     * Sets the value of the changeState property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeFlag }
     *     
     */
    public void setChangeState(ChangeFlag value) {
        this.changeState = value;
    }

    /**
     * Gets the value of the contactAddress property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfContactAddress }
     *     
     */
    public ArrayOfContactAddress getContactAddress() {
        return contactAddress;
    }

    /**
     * Sets the value of the contactAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfContactAddress }
     *     
     */
    public void setContactAddress(ArrayOfContactAddress value) {
        this.contactAddress = value;
    }

    /**
     * Gets the value of the contactTelephone property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfContactTelephone }
     *     
     */
    public ArrayOfContactTelephone getContactTelephone() {
        return contactTelephone;
    }

    /**
     * Sets the value of the contactTelephone property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfContactTelephone }
     *     
     */
    public void setContactTelephone(ArrayOfContactTelephone value) {
        this.contactTelephone = value;
    }

    /**
     * Gets the value of the contactRole property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfContactRole }
     *     
     */
    public ArrayOfContactRole getContactRole() {
        return contactRole;
    }

    /**
     * Sets the value of the contactRole property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfContactRole }
     *     
     */
    public void setContactRole(ArrayOfContactRole value) {
        this.contactRole = value;
    }

    /**
     * Gets the value of the contactComment property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfContactComment }
     *     
     */
    public ArrayOfContactComment getContactComment() {
        return contactComment;
    }

    /**
     * Sets the value of the contactComment property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfContactComment }
     *     
     */
    public void setContactComment(ArrayOfContactComment value) {
        this.contactComment = value;
    }

}
