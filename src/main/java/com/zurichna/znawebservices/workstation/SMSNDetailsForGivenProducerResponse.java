
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SMSNDetailsForGivenProducerResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SMSNDetailsForGivenProducerResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}ResponseBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="NEXT_ITR" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="smsnDetails" type="{http://workstation.znawebservices.zurichna.com}ArrayOfSubmissionDetails" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SMSNDetailsForGivenProducerResponse", propOrder = {
    "nextitr",
    "smsnDetails"
})
public class SMSNDetailsForGivenProducerResponse
    extends ResponseBase
{

    @XmlElement(name = "NEXT_ITR")
    protected boolean nextitr;
    protected ArrayOfSubmissionDetails smsnDetails;

    /**
     * Gets the value of the nextitr property.
     * 
     */
    public boolean isNEXTITR() {
        return nextitr;
    }

    /**
     * Sets the value of the nextitr property.
     * 
     */
    public void setNEXTITR(boolean value) {
        this.nextitr = value;
    }

    /**
     * Gets the value of the smsnDetails property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfSubmissionDetails }
     *     
     */
    public ArrayOfSubmissionDetails getSmsnDetails() {
        return smsnDetails;
    }

    /**
     * Sets the value of the smsnDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfSubmissionDetails }
     *     
     */
    public void setSmsnDetails(ArrayOfSubmissionDetails value) {
        this.smsnDetails = value;
    }

}
