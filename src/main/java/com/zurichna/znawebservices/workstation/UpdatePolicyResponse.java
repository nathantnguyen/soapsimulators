
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UpdatePolicyResult" type="{http://workstation.znawebservices.zurichna.com}PolicyResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "updatePolicyResult"
})
@XmlRootElement(name = "UpdatePolicyResponse")
public class UpdatePolicyResponse {

    @XmlElement(name = "UpdatePolicyResult")
    protected PolicyResponse updatePolicyResult;

    /**
     * Gets the value of the updatePolicyResult property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyResponse }
     *     
     */
    public PolicyResponse getUpdatePolicyResult() {
        return updatePolicyResult;
    }

    /**
     * Sets the value of the updatePolicyResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyResponse }
     *     
     */
    public void setUpdatePolicyResult(PolicyResponse value) {
        this.updatePolicyResult = value;
    }

}
