
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubmissionBindResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubmissionBindResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}ResponseBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="masterPolicy" type="{http://workstation.znawebservices.zurichna.com}MasterPolicy" minOccurs="0"/&gt;
 *         &lt;element name="submission" type="{http://workstation.znawebservices.zurichna.com}SubmissionBind" minOccurs="0"/&gt;
 *         &lt;element name="policy" type="{http://workstation.znawebservices.zurichna.com}ArrayOfPolicys" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmissionBindResponse", propOrder = {
    "masterPolicy",
    "submission",
    "policy"
})
public class SubmissionBindResponse
    extends ResponseBase
{

    protected MasterPolicy masterPolicy;
    protected SubmissionBind submission;
    protected ArrayOfPolicys policy;

    /**
     * Gets the value of the masterPolicy property.
     * 
     * @return
     *     possible object is
     *     {@link MasterPolicy }
     *     
     */
    public MasterPolicy getMasterPolicy() {
        return masterPolicy;
    }

    /**
     * Sets the value of the masterPolicy property.
     * 
     * @param value
     *     allowed object is
     *     {@link MasterPolicy }
     *     
     */
    public void setMasterPolicy(MasterPolicy value) {
        this.masterPolicy = value;
    }

    /**
     * Gets the value of the submission property.
     * 
     * @return
     *     possible object is
     *     {@link SubmissionBind }
     *     
     */
    public SubmissionBind getSubmission() {
        return submission;
    }

    /**
     * Sets the value of the submission property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubmissionBind }
     *     
     */
    public void setSubmission(SubmissionBind value) {
        this.submission = value;
    }

    /**
     * Gets the value of the policy property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfPolicys }
     *     
     */
    public ArrayOfPolicys getPolicy() {
        return policy;
    }

    /**
     * Sets the value of the policy property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfPolicys }
     *     
     */
    public void setPolicy(ArrayOfPolicys value) {
        this.policy = value;
    }

}
