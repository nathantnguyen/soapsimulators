
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetTargetPriceRateTrackDtlsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetTargetPriceRateTrackDtlsResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}ResponseBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ResponseData" type="{http://workstation.znawebservices.zurichna.com}ArrayOfTargetPriceRateTrackDtls" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetTargetPriceRateTrackDtlsResponse", propOrder = {
    "responseData"
})
public class GetTargetPriceRateTrackDtlsResponse2
    extends ResponseBase
{

    @XmlElement(name = "ResponseData")
    protected ArrayOfTargetPriceRateTrackDtls responseData;

    /**
     * Gets the value of the responseData property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTargetPriceRateTrackDtls }
     *     
     */
    public ArrayOfTargetPriceRateTrackDtls getResponseData() {
        return responseData;
    }

    /**
     * Sets the value of the responseData property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTargetPriceRateTrackDtls }
     *     
     */
    public void setResponseData(ArrayOfTargetPriceRateTrackDtls value) {
        this.responseData = value;
    }

}
