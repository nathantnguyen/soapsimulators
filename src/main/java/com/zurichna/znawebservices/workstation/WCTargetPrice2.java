
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WCTargetPrice complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WCTargetPrice"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ProgramsModelPrc18PctROE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ProgramsIndPrc18PctROE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ProgramsIndLowPct" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ProgramsIndHighPct" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TargetPriceErrMsg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="successfullScoreIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ThreeYearsLossRatio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ClaimFrequencyValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DemographicData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="HistoricData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RiskComplexity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FinancialHealth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PolicyTenure" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Decile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WCTargetPrice", propOrder = {
    "programsModelPrc18PctROE",
    "programsIndPrc18PctROE",
    "programsIndLowPct",
    "programsIndHighPct",
    "targetPriceErrMsg",
    "successfullScoreIndicator",
    "threeYearsLossRatio",
    "claimFrequencyValue",
    "demographicData",
    "historicData",
    "riskComplexity",
    "financialHealth",
    "policyTenure",
    "decile"
})
public class WCTargetPrice2 {

    @XmlElement(name = "ProgramsModelPrc18PctROE")
    protected String programsModelPrc18PctROE;
    @XmlElement(name = "ProgramsIndPrc18PctROE")
    protected String programsIndPrc18PctROE;
    @XmlElement(name = "ProgramsIndLowPct")
    protected String programsIndLowPct;
    @XmlElement(name = "ProgramsIndHighPct")
    protected String programsIndHighPct;
    @XmlElement(name = "TargetPriceErrMsg")
    protected String targetPriceErrMsg;
    protected String successfullScoreIndicator;
    @XmlElement(name = "ThreeYearsLossRatio")
    protected String threeYearsLossRatio;
    @XmlElement(name = "ClaimFrequencyValue")
    protected String claimFrequencyValue;
    @XmlElement(name = "DemographicData")
    protected String demographicData;
    @XmlElement(name = "HistoricData")
    protected String historicData;
    @XmlElement(name = "RiskComplexity")
    protected String riskComplexity;
    @XmlElement(name = "FinancialHealth")
    protected String financialHealth;
    @XmlElement(name = "PolicyTenure")
    protected String policyTenure;
    @XmlElement(name = "Decile")
    protected String decile;

    /**
     * Gets the value of the programsModelPrc18PctROE property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProgramsModelPrc18PctROE() {
        return programsModelPrc18PctROE;
    }

    /**
     * Sets the value of the programsModelPrc18PctROE property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProgramsModelPrc18PctROE(String value) {
        this.programsModelPrc18PctROE = value;
    }

    /**
     * Gets the value of the programsIndPrc18PctROE property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProgramsIndPrc18PctROE() {
        return programsIndPrc18PctROE;
    }

    /**
     * Sets the value of the programsIndPrc18PctROE property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProgramsIndPrc18PctROE(String value) {
        this.programsIndPrc18PctROE = value;
    }

    /**
     * Gets the value of the programsIndLowPct property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProgramsIndLowPct() {
        return programsIndLowPct;
    }

    /**
     * Sets the value of the programsIndLowPct property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProgramsIndLowPct(String value) {
        this.programsIndLowPct = value;
    }

    /**
     * Gets the value of the programsIndHighPct property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProgramsIndHighPct() {
        return programsIndHighPct;
    }

    /**
     * Sets the value of the programsIndHighPct property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProgramsIndHighPct(String value) {
        this.programsIndHighPct = value;
    }

    /**
     * Gets the value of the targetPriceErrMsg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetPriceErrMsg() {
        return targetPriceErrMsg;
    }

    /**
     * Sets the value of the targetPriceErrMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetPriceErrMsg(String value) {
        this.targetPriceErrMsg = value;
    }

    /**
     * Gets the value of the successfullScoreIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSuccessfullScoreIndicator() {
        return successfullScoreIndicator;
    }

    /**
     * Sets the value of the successfullScoreIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSuccessfullScoreIndicator(String value) {
        this.successfullScoreIndicator = value;
    }

    /**
     * Gets the value of the threeYearsLossRatio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getThreeYearsLossRatio() {
        return threeYearsLossRatio;
    }

    /**
     * Sets the value of the threeYearsLossRatio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setThreeYearsLossRatio(String value) {
        this.threeYearsLossRatio = value;
    }

    /**
     * Gets the value of the claimFrequencyValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClaimFrequencyValue() {
        return claimFrequencyValue;
    }

    /**
     * Sets the value of the claimFrequencyValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClaimFrequencyValue(String value) {
        this.claimFrequencyValue = value;
    }

    /**
     * Gets the value of the demographicData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDemographicData() {
        return demographicData;
    }

    /**
     * Sets the value of the demographicData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDemographicData(String value) {
        this.demographicData = value;
    }

    /**
     * Gets the value of the historicData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHistoricData() {
        return historicData;
    }

    /**
     * Sets the value of the historicData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHistoricData(String value) {
        this.historicData = value;
    }

    /**
     * Gets the value of the riskComplexity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRiskComplexity() {
        return riskComplexity;
    }

    /**
     * Sets the value of the riskComplexity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiskComplexity(String value) {
        this.riskComplexity = value;
    }

    /**
     * Gets the value of the financialHealth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFinancialHealth() {
        return financialHealth;
    }

    /**
     * Sets the value of the financialHealth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFinancialHealth(String value) {
        this.financialHealth = value;
    }

    /**
     * Gets the value of the policyTenure property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyTenure() {
        return policyTenure;
    }

    /**
     * Sets the value of the policyTenure property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyTenure(String value) {
        this.policyTenure = value;
    }

    /**
     * Gets the value of the decile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDecile() {
        return decile;
    }

    /**
     * Sets the value of the decile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDecile(String value) {
        this.decile = value;
    }

}
