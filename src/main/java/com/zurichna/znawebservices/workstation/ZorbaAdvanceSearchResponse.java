
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ZorbaAdvanceSearchResult" type="{http://workstation.znawebservices.zurichna.com}AdvanceZorbaSearchResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "zorbaAdvanceSearchResult"
})
@XmlRootElement(name = "ZorbaAdvanceSearchResponse")
public class ZorbaAdvanceSearchResponse {

    @XmlElement(name = "ZorbaAdvanceSearchResult")
    protected AdvanceZorbaSearchResponse zorbaAdvanceSearchResult;

    /**
     * Gets the value of the zorbaAdvanceSearchResult property.
     * 
     * @return
     *     possible object is
     *     {@link AdvanceZorbaSearchResponse }
     *     
     */
    public AdvanceZorbaSearchResponse getZorbaAdvanceSearchResult() {
        return zorbaAdvanceSearchResult;
    }

    /**
     * Sets the value of the zorbaAdvanceSearchResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdvanceZorbaSearchResponse }
     *     
     */
    public void setZorbaAdvanceSearchResult(AdvanceZorbaSearchResponse value) {
        this.zorbaAdvanceSearchResult = value;
    }

}
