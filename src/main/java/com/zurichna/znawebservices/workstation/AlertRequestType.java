
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AlertRequestType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AlertRequestType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="UserId"/&gt;
 *     &lt;enumeration value="UserIdSearchArchive"/&gt;
 *     &lt;enumeration value="AccountId"/&gt;
 *     &lt;enumeration value="AccountIdSearchArchive"/&gt;
 *     &lt;enumeration value="GlobalByUserId"/&gt;
 *     &lt;enumeration value="GlobalByUserIdSearchArchive"/&gt;
 *     &lt;enumeration value="All"/&gt;
 *     &lt;enumeration value="AllSearchArchive"/&gt;
 *     &lt;enumeration value="EventId"/&gt;
 *     &lt;enumeration value="EventIdSearchArchive"/&gt;
 *     &lt;enumeration value="EventRootId"/&gt;
 *     &lt;enumeration value="EventRootIdSearchArchive"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "AlertRequestType")
@XmlEnum
public enum AlertRequestType {

    @XmlEnumValue("UserId")
    USER_ID("UserId"),
    @XmlEnumValue("UserIdSearchArchive")
    USER_ID_SEARCH_ARCHIVE("UserIdSearchArchive"),
    @XmlEnumValue("AccountId")
    ACCOUNT_ID("AccountId"),
    @XmlEnumValue("AccountIdSearchArchive")
    ACCOUNT_ID_SEARCH_ARCHIVE("AccountIdSearchArchive"),
    @XmlEnumValue("GlobalByUserId")
    GLOBAL_BY_USER_ID("GlobalByUserId"),
    @XmlEnumValue("GlobalByUserIdSearchArchive")
    GLOBAL_BY_USER_ID_SEARCH_ARCHIVE("GlobalByUserIdSearchArchive"),
    @XmlEnumValue("All")
    ALL("All"),
    @XmlEnumValue("AllSearchArchive")
    ALL_SEARCH_ARCHIVE("AllSearchArchive"),
    @XmlEnumValue("EventId")
    EVENT_ID("EventId"),
    @XmlEnumValue("EventIdSearchArchive")
    EVENT_ID_SEARCH_ARCHIVE("EventIdSearchArchive"),
    @XmlEnumValue("EventRootId")
    EVENT_ROOT_ID("EventRootId"),
    @XmlEnumValue("EventRootIdSearchArchive")
    EVENT_ROOT_ID_SEARCH_ARCHIVE("EventRootIdSearchArchive");
    private final String value;

    AlertRequestType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AlertRequestType fromValue(String v) {
        for (AlertRequestType c: AlertRequestType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
