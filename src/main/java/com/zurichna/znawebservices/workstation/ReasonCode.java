
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReasonCode complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReasonCode"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="STS_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RSN_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RSN_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReasonCode", propOrder = {
    "stscd",
    "rsncd",
    "rsndesc"
})
public class ReasonCode {

    @XmlElement(name = "STS_CD")
    protected String stscd;
    @XmlElement(name = "RSN_CD")
    protected String rsncd;
    @XmlElement(name = "RSN_DESC")
    protected String rsndesc;

    /**
     * Gets the value of the stscd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTSCD() {
        return stscd;
    }

    /**
     * Sets the value of the stscd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTSCD(String value) {
        this.stscd = value;
    }

    /**
     * Gets the value of the rsncd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRSNCD() {
        return rsncd;
    }

    /**
     * Sets the value of the rsncd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRSNCD(String value) {
        this.rsncd = value;
    }

    /**
     * Gets the value of the rsndesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRSNDESC() {
        return rsndesc;
    }

    /**
     * Sets the value of the rsndesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRSNDESC(String value) {
        this.rsndesc = value;
    }

}
