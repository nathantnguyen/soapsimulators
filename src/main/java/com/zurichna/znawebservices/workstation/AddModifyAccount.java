
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for AddModifyAccount complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AddModifyAccount"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ZST_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ENPRS_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SITE_DUNS_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ACCT_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PTY_TYP_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PTY_ROL_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="EFF_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="EXPI_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="ENT_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="ADDR_CHG_FLG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NAME_CHG_FLG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="address" type="{http://workstation.znawebservices.zurichna.com}ArrayOfAddress" minOccurs="0"/&gt;
 *         &lt;element name="customerAdditionalInformation" type="{http://workstation.znawebservices.zurichna.com}ArrayOfCustomerAdditionalInformation" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddModifyAccount", propOrder = {
    "zstnbr",
    "enprsid",
    "sitedunsnbr",
    "acctnm",
    "ptytypid",
    "ptyrolid",
    "effdt",
    "expidt",
    "enttistmp",
    "addrchgflg",
    "namechgflg",
    "address",
    "customerAdditionalInformation"
})
public class AddModifyAccount {

    @XmlElement(name = "ZST_NBR")
    protected String zstnbr;
    @XmlElement(name = "ENPRS_ID")
    protected String enprsid;
    @XmlElement(name = "SITE_DUNS_NBR")
    protected String sitedunsnbr;
    @XmlElement(name = "ACCT_NM")
    protected String acctnm;
    @XmlElement(name = "PTY_TYP_ID")
    protected int ptytypid;
    @XmlElement(name = "PTY_ROL_ID")
    protected int ptyrolid;
    @XmlElement(name = "EFF_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effdt;
    @XmlElement(name = "EXPI_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expidt;
    @XmlElement(name = "ENT_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar enttistmp;
    @XmlElement(name = "ADDR_CHG_FLG")
    protected String addrchgflg;
    @XmlElement(name = "NAME_CHG_FLG")
    protected String namechgflg;
    protected ArrayOfAddress address;
    protected ArrayOfCustomerAdditionalInformation customerAdditionalInformation;

    /**
     * Gets the value of the zstnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZSTNBR() {
        return zstnbr;
    }

    /**
     * Sets the value of the zstnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZSTNBR(String value) {
        this.zstnbr = value;
    }

    /**
     * Gets the value of the enprsid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getENPRSID() {
        return enprsid;
    }

    /**
     * Sets the value of the enprsid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setENPRSID(String value) {
        this.enprsid = value;
    }

    /**
     * Gets the value of the sitedunsnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSITEDUNSNBR() {
        return sitedunsnbr;
    }

    /**
     * Sets the value of the sitedunsnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSITEDUNSNBR(String value) {
        this.sitedunsnbr = value;
    }

    /**
     * Gets the value of the acctnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACCTNM() {
        return acctnm;
    }

    /**
     * Sets the value of the acctnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACCTNM(String value) {
        this.acctnm = value;
    }

    /**
     * Gets the value of the ptytypid property.
     * 
     */
    public int getPTYTYPID() {
        return ptytypid;
    }

    /**
     * Sets the value of the ptytypid property.
     * 
     */
    public void setPTYTYPID(int value) {
        this.ptytypid = value;
    }

    /**
     * Gets the value of the ptyrolid property.
     * 
     */
    public int getPTYROLID() {
        return ptyrolid;
    }

    /**
     * Sets the value of the ptyrolid property.
     * 
     */
    public void setPTYROLID(int value) {
        this.ptyrolid = value;
    }

    /**
     * Gets the value of the effdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEFFDT() {
        return effdt;
    }

    /**
     * Sets the value of the effdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEFFDT(XMLGregorianCalendar value) {
        this.effdt = value;
    }

    /**
     * Gets the value of the expidt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEXPIDT() {
        return expidt;
    }

    /**
     * Sets the value of the expidt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEXPIDT(XMLGregorianCalendar value) {
        this.expidt = value;
    }

    /**
     * Gets the value of the enttistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getENTTISTMP() {
        return enttistmp;
    }

    /**
     * Sets the value of the enttistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setENTTISTMP(XMLGregorianCalendar value) {
        this.enttistmp = value;
    }

    /**
     * Gets the value of the addrchgflg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getADDRCHGFLG() {
        return addrchgflg;
    }

    /**
     * Sets the value of the addrchgflg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setADDRCHGFLG(String value) {
        this.addrchgflg = value;
    }

    /**
     * Gets the value of the namechgflg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNAMECHGFLG() {
        return namechgflg;
    }

    /**
     * Sets the value of the namechgflg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNAMECHGFLG(String value) {
        this.namechgflg = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfAddress }
     *     
     */
    public ArrayOfAddress getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfAddress }
     *     
     */
    public void setAddress(ArrayOfAddress value) {
        this.address = value;
    }

    /**
     * Gets the value of the customerAdditionalInformation property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfCustomerAdditionalInformation }
     *     
     */
    public ArrayOfCustomerAdditionalInformation getCustomerAdditionalInformation() {
        return customerAdditionalInformation;
    }

    /**
     * Sets the value of the customerAdditionalInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfCustomerAdditionalInformation }
     *     
     */
    public void setCustomerAdditionalInformation(ArrayOfCustomerAdditionalInformation value) {
        this.customerAdditionalInformation = value;
    }

}
