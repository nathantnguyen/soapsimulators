
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RenewPolicyResult" type="{http://workstation.znawebservices.zurichna.com}RenewPolicyInfoResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "renewPolicyResult"
})
@XmlRootElement(name = "RenewPolicyResponse")
public class RenewPolicyResponse {

    @XmlElement(name = "RenewPolicyResult")
    protected RenewPolicyInfoResponse renewPolicyResult;

    /**
     * Gets the value of the renewPolicyResult property.
     * 
     * @return
     *     possible object is
     *     {@link RenewPolicyInfoResponse }
     *     
     */
    public RenewPolicyInfoResponse getRenewPolicyResult() {
        return renewPolicyResult;
    }

    /**
     * Sets the value of the renewPolicyResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link RenewPolicyInfoResponse }
     *     
     */
    public void setRenewPolicyResult(RenewPolicyInfoResponse value) {
        this.renewPolicyResult = value;
    }

}
