
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReferenceRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReferenceRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}RequestBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="referenceRequestType" type="{http://workstation.znawebservices.zurichna.com}ReferenceRequestType"/&gt;
 *         &lt;element name="requestCriteria" type="{http://workstation.znawebservices.zurichna.com}ArrayOfReferenceRequestCriteria" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReferenceRequest", propOrder = {
    "referenceRequestType",
    "requestCriteria"
})
public class ReferenceRequest
    extends RequestBase
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ReferenceRequestType referenceRequestType;
    protected ArrayOfReferenceRequestCriteria requestCriteria;

    /**
     * Gets the value of the referenceRequestType property.
     * 
     * @return
     *     possible object is
     *     {@link ReferenceRequestType }
     *     
     */
    public ReferenceRequestType getReferenceRequestType() {
        return referenceRequestType;
    }

    /**
     * Sets the value of the referenceRequestType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferenceRequestType }
     *     
     */
    public void setReferenceRequestType(ReferenceRequestType value) {
        this.referenceRequestType = value;
    }

    /**
     * Gets the value of the requestCriteria property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfReferenceRequestCriteria }
     *     
     */
    public ArrayOfReferenceRequestCriteria getRequestCriteria() {
        return requestCriteria;
    }

    /**
     * Sets the value of the requestCriteria property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfReferenceRequestCriteria }
     *     
     */
    public void setRequestCriteria(ArrayOfReferenceRequestCriteria value) {
        this.requestCriteria = value;
    }

}
