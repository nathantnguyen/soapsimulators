
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetMVRDataResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetMVRDataResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}ResponseBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SMSN_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="submissionProduct" type="{http://workstation.znawebservices.zurichna.com}ArrayOfMVRDataSubmissionProduct" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetMVRDataResponse", propOrder = {
    "smsnid",
    "submissionProduct"
})
public class GetMVRDataResponse2
    extends ResponseBase
{

    @XmlElement(name = "SMSN_ID")
    protected int smsnid;
    protected ArrayOfMVRDataSubmissionProduct submissionProduct;

    /**
     * Gets the value of the smsnid property.
     * 
     */
    public int getSMSNID() {
        return smsnid;
    }

    /**
     * Sets the value of the smsnid property.
     * 
     */
    public void setSMSNID(int value) {
        this.smsnid = value;
    }

    /**
     * Gets the value of the submissionProduct property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfMVRDataSubmissionProduct }
     *     
     */
    public ArrayOfMVRDataSubmissionProduct getSubmissionProduct() {
        return submissionProduct;
    }

    /**
     * Sets the value of the submissionProduct property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfMVRDataSubmissionProduct }
     *     
     */
    public void setSubmissionProduct(ArrayOfMVRDataSubmissionProduct value) {
        this.submissionProduct = value;
    }

}
