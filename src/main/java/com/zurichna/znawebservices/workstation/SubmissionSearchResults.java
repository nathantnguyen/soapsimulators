
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for SubmissionSearchResults complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubmissionSearchResults"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CUST_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="CUST_CRMS_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="LST_ALRT_IND" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="LST_ALRT_TTL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CUST_PTY_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SMSN_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="RECD_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="CUSTM_INS_SPEC_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="TMPL_INS_SPEC_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="TMPL_INS_SPEC_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TMPL_INS_SPEC_PRDT_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RQST_COVG_EFF_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="RQST_COVG_EXPI_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="SIC_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SIC_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UNDG_PGM_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="UNDG_PGM_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UNDG_PGM_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="PRDR_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORGL_PRDR_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_NBR_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_STS_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PRDR_STS_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UNDR_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="UNDR_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORG_PTY_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ORG_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORG_NM_COMB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORG_ABBR_COMB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORG_BU_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ORG_BU_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EXPO_LOC_CLS_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EXPO_LOC_CLS_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NEW_RENL_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NEW_RENL_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CUR_STS_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CUR_STS_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CUR_RSN_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CUR_RSN_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CUR_STS_TISTMP" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmissionSearchResults", propOrder = {
    "custptyid",
    "custcrmsptyid",
    "lstalrtind",
    "lstalrtttl",
    "custptynm",
    "smsnid",
    "recddt",
    "custminsspecid",
    "tmplinsspecid",
    "tmplinsspecnm",
    "tmplinsspecprdtabbr",
    "rqstcovgeffdt",
    "rqstcovgexpidt",
    "siccd",
    "sicnm",
    "undgpgmid",
    "undgpgmcd",
    "undgpgmnm",
    "prdrptyid",
    "prdrnbr",
    "orglprdrnbr",
    "prdrnm",
    "prdrnbrnm",
    "prdrstscd",
    "prdrstsnm",
    "undrptyid",
    "undrnm",
    "orgptyid",
    "orgnm",
    "orgnmcomb",
    "orgabbrcomb",
    "orgbunm",
    "orgbuabbr",
    "expolocclscd",
    "expolocclsnm",
    "newrenlcd",
    "newrenlnm",
    "curstscd",
    "curstsnm",
    "currsncd",
    "currsnnm",
    "curststistmp"
})
public class SubmissionSearchResults {

    @XmlElement(name = "CUST_PTY_ID")
    protected int custptyid;
    @XmlElement(name = "CUST_CRMS_PTY_ID")
    protected int custcrmsptyid;
    @XmlElement(name = "LST_ALRT_IND")
    protected boolean lstalrtind;
    @XmlElement(name = "LST_ALRT_TTL")
    protected String lstalrtttl;
    @XmlElement(name = "CUST_PTY_NM")
    protected String custptynm;
    @XmlElement(name = "SMSN_ID")
    protected int smsnid;
    @XmlElement(name = "RECD_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar recddt;
    @XmlElement(name = "CUSTM_INS_SPEC_ID")
    protected int custminsspecid;
    @XmlElement(name = "TMPL_INS_SPEC_ID")
    protected int tmplinsspecid;
    @XmlElement(name = "TMPL_INS_SPEC_NM")
    protected String tmplinsspecnm;
    @XmlElement(name = "TMPL_INS_SPEC_PRDT_ABBR")
    protected String tmplinsspecprdtabbr;
    @XmlElement(name = "RQST_COVG_EFF_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar rqstcovgeffdt;
    @XmlElement(name = "RQST_COVG_EXPI_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar rqstcovgexpidt;
    @XmlElement(name = "SIC_CD")
    protected String siccd;
    @XmlElement(name = "SIC_NM")
    protected String sicnm;
    @XmlElement(name = "UNDG_PGM_ID")
    protected int undgpgmid;
    @XmlElement(name = "UNDG_PGM_CD")
    protected String undgpgmcd;
    @XmlElement(name = "UNDG_PGM_NM")
    protected String undgpgmnm;
    @XmlElement(name = "PRDR_PTY_ID")
    protected int prdrptyid;
    @XmlElement(name = "PRDR_NBR")
    protected String prdrnbr;
    @XmlElement(name = "ORGL_PRDR_NBR")
    protected String orglprdrnbr;
    @XmlElement(name = "PRDR_NM")
    protected String prdrnm;
    @XmlElement(name = "PRDR_NBR_NM")
    protected String prdrnbrnm;
    @XmlElement(name = "PRDR_STS_CD")
    protected String prdrstscd;
    @XmlElement(name = "PRDR_STS_NM")
    protected String prdrstsnm;
    @XmlElement(name = "UNDR_PTY_ID")
    protected int undrptyid;
    @XmlElement(name = "UNDR_NM")
    protected String undrnm;
    @XmlElement(name = "ORG_PTY_ID")
    protected int orgptyid;
    @XmlElement(name = "ORG_NM")
    protected String orgnm;
    @XmlElement(name = "ORG_NM_COMB")
    protected String orgnmcomb;
    @XmlElement(name = "ORG_ABBR_COMB")
    protected String orgabbrcomb;
    @XmlElement(name = "ORG_BU_NM")
    protected String orgbunm;
    @XmlElement(name = "ORG_BU_ABBR")
    protected String orgbuabbr;
    @XmlElement(name = "EXPO_LOC_CLS_CD")
    protected String expolocclscd;
    @XmlElement(name = "EXPO_LOC_CLS_NM")
    protected String expolocclsnm;
    @XmlElement(name = "NEW_RENL_CD")
    protected String newrenlcd;
    @XmlElement(name = "NEW_RENL_NM")
    protected String newrenlnm;
    @XmlElement(name = "CUR_STS_CD")
    protected String curstscd;
    @XmlElement(name = "CUR_STS_NM")
    protected String curstsnm;
    @XmlElement(name = "CUR_RSN_CD")
    protected String currsncd;
    @XmlElement(name = "CUR_RSN_NM")
    protected String currsnnm;
    @XmlElement(name = "CUR_STS_TISTMP", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar curststistmp;

    /**
     * Gets the value of the custptyid property.
     * 
     */
    public int getCUSTPTYID() {
        return custptyid;
    }

    /**
     * Sets the value of the custptyid property.
     * 
     */
    public void setCUSTPTYID(int value) {
        this.custptyid = value;
    }

    /**
     * Gets the value of the custcrmsptyid property.
     * 
     */
    public int getCUSTCRMSPTYID() {
        return custcrmsptyid;
    }

    /**
     * Sets the value of the custcrmsptyid property.
     * 
     */
    public void setCUSTCRMSPTYID(int value) {
        this.custcrmsptyid = value;
    }

    /**
     * Gets the value of the lstalrtind property.
     * 
     */
    public boolean isLSTALRTIND() {
        return lstalrtind;
    }

    /**
     * Sets the value of the lstalrtind property.
     * 
     */
    public void setLSTALRTIND(boolean value) {
        this.lstalrtind = value;
    }

    /**
     * Gets the value of the lstalrtttl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLSTALRTTTL() {
        return lstalrtttl;
    }

    /**
     * Sets the value of the lstalrtttl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLSTALRTTTL(String value) {
        this.lstalrtttl = value;
    }

    /**
     * Gets the value of the custptynm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCUSTPTYNM() {
        return custptynm;
    }

    /**
     * Sets the value of the custptynm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCUSTPTYNM(String value) {
        this.custptynm = value;
    }

    /**
     * Gets the value of the smsnid property.
     * 
     */
    public int getSMSNID() {
        return smsnid;
    }

    /**
     * Sets the value of the smsnid property.
     * 
     */
    public void setSMSNID(int value) {
        this.smsnid = value;
    }

    /**
     * Gets the value of the recddt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRECDDT() {
        return recddt;
    }

    /**
     * Sets the value of the recddt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRECDDT(XMLGregorianCalendar value) {
        this.recddt = value;
    }

    /**
     * Gets the value of the custminsspecid property.
     * 
     */
    public int getCUSTMINSSPECID() {
        return custminsspecid;
    }

    /**
     * Sets the value of the custminsspecid property.
     * 
     */
    public void setCUSTMINSSPECID(int value) {
        this.custminsspecid = value;
    }

    /**
     * Gets the value of the tmplinsspecid property.
     * 
     */
    public int getTMPLINSSPECID() {
        return tmplinsspecid;
    }

    /**
     * Sets the value of the tmplinsspecid property.
     * 
     */
    public void setTMPLINSSPECID(int value) {
        this.tmplinsspecid = value;
    }

    /**
     * Gets the value of the tmplinsspecnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTMPLINSSPECNM() {
        return tmplinsspecnm;
    }

    /**
     * Sets the value of the tmplinsspecnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTMPLINSSPECNM(String value) {
        this.tmplinsspecnm = value;
    }

    /**
     * Gets the value of the tmplinsspecprdtabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTMPLINSSPECPRDTABBR() {
        return tmplinsspecprdtabbr;
    }

    /**
     * Sets the value of the tmplinsspecprdtabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTMPLINSSPECPRDTABBR(String value) {
        this.tmplinsspecprdtabbr = value;
    }

    /**
     * Gets the value of the rqstcovgeffdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRQSTCOVGEFFDT() {
        return rqstcovgeffdt;
    }

    /**
     * Sets the value of the rqstcovgeffdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRQSTCOVGEFFDT(XMLGregorianCalendar value) {
        this.rqstcovgeffdt = value;
    }

    /**
     * Gets the value of the rqstcovgexpidt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRQSTCOVGEXPIDT() {
        return rqstcovgexpidt;
    }

    /**
     * Sets the value of the rqstcovgexpidt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRQSTCOVGEXPIDT(XMLGregorianCalendar value) {
        this.rqstcovgexpidt = value;
    }

    /**
     * Gets the value of the siccd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSICCD() {
        return siccd;
    }

    /**
     * Sets the value of the siccd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSICCD(String value) {
        this.siccd = value;
    }

    /**
     * Gets the value of the sicnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSICNM() {
        return sicnm;
    }

    /**
     * Sets the value of the sicnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSICNM(String value) {
        this.sicnm = value;
    }

    /**
     * Gets the value of the undgpgmid property.
     * 
     */
    public int getUNDGPGMID() {
        return undgpgmid;
    }

    /**
     * Sets the value of the undgpgmid property.
     * 
     */
    public void setUNDGPGMID(int value) {
        this.undgpgmid = value;
    }

    /**
     * Gets the value of the undgpgmcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDGPGMCD() {
        return undgpgmcd;
    }

    /**
     * Sets the value of the undgpgmcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDGPGMCD(String value) {
        this.undgpgmcd = value;
    }

    /**
     * Gets the value of the undgpgmnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDGPGMNM() {
        return undgpgmnm;
    }

    /**
     * Sets the value of the undgpgmnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDGPGMNM(String value) {
        this.undgpgmnm = value;
    }

    /**
     * Gets the value of the prdrptyid property.
     * 
     */
    public int getPRDRPTYID() {
        return prdrptyid;
    }

    /**
     * Sets the value of the prdrptyid property.
     * 
     */
    public void setPRDRPTYID(int value) {
        this.prdrptyid = value;
    }

    /**
     * Gets the value of the prdrnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRNBR() {
        return prdrnbr;
    }

    /**
     * Sets the value of the prdrnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRNBR(String value) {
        this.prdrnbr = value;
    }

    /**
     * Gets the value of the orglprdrnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGLPRDRNBR() {
        return orglprdrnbr;
    }

    /**
     * Sets the value of the orglprdrnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGLPRDRNBR(String value) {
        this.orglprdrnbr = value;
    }

    /**
     * Gets the value of the prdrnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRNM() {
        return prdrnm;
    }

    /**
     * Sets the value of the prdrnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRNM(String value) {
        this.prdrnm = value;
    }

    /**
     * Gets the value of the prdrnbrnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRNBRNM() {
        return prdrnbrnm;
    }

    /**
     * Sets the value of the prdrnbrnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRNBRNM(String value) {
        this.prdrnbrnm = value;
    }

    /**
     * Gets the value of the prdrstscd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRSTSCD() {
        return prdrstscd;
    }

    /**
     * Sets the value of the prdrstscd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRSTSCD(String value) {
        this.prdrstscd = value;
    }

    /**
     * Gets the value of the prdrstsnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRDRSTSNM() {
        return prdrstsnm;
    }

    /**
     * Sets the value of the prdrstsnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRDRSTSNM(String value) {
        this.prdrstsnm = value;
    }

    /**
     * Gets the value of the undrptyid property.
     * 
     */
    public int getUNDRPTYID() {
        return undrptyid;
    }

    /**
     * Sets the value of the undrptyid property.
     * 
     */
    public void setUNDRPTYID(int value) {
        this.undrptyid = value;
    }

    /**
     * Gets the value of the undrnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNDRNM() {
        return undrnm;
    }

    /**
     * Sets the value of the undrnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNDRNM(String value) {
        this.undrnm = value;
    }

    /**
     * Gets the value of the orgptyid property.
     * 
     */
    public int getORGPTYID() {
        return orgptyid;
    }

    /**
     * Sets the value of the orgptyid property.
     * 
     */
    public void setORGPTYID(int value) {
        this.orgptyid = value;
    }

    /**
     * Gets the value of the orgnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGNM() {
        return orgnm;
    }

    /**
     * Sets the value of the orgnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGNM(String value) {
        this.orgnm = value;
    }

    /**
     * Gets the value of the orgnmcomb property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGNMCOMB() {
        return orgnmcomb;
    }

    /**
     * Sets the value of the orgnmcomb property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGNMCOMB(String value) {
        this.orgnmcomb = value;
    }

    /**
     * Gets the value of the orgabbrcomb property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGABBRCOMB() {
        return orgabbrcomb;
    }

    /**
     * Sets the value of the orgabbrcomb property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGABBRCOMB(String value) {
        this.orgabbrcomb = value;
    }

    /**
     * Gets the value of the orgbunm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGBUNM() {
        return orgbunm;
    }

    /**
     * Sets the value of the orgbunm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGBUNM(String value) {
        this.orgbunm = value;
    }

    /**
     * Gets the value of the orgbuabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORGBUABBR() {
        return orgbuabbr;
    }

    /**
     * Sets the value of the orgbuabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORGBUABBR(String value) {
        this.orgbuabbr = value;
    }

    /**
     * Gets the value of the expolocclscd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEXPOLOCCLSCD() {
        return expolocclscd;
    }

    /**
     * Sets the value of the expolocclscd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEXPOLOCCLSCD(String value) {
        this.expolocclscd = value;
    }

    /**
     * Gets the value of the expolocclsnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEXPOLOCCLSNM() {
        return expolocclsnm;
    }

    /**
     * Sets the value of the expolocclsnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEXPOLOCCLSNM(String value) {
        this.expolocclsnm = value;
    }

    /**
     * Gets the value of the newrenlcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNEWRENLCD() {
        return newrenlcd;
    }

    /**
     * Sets the value of the newrenlcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNEWRENLCD(String value) {
        this.newrenlcd = value;
    }

    /**
     * Gets the value of the newrenlnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNEWRENLNM() {
        return newrenlnm;
    }

    /**
     * Sets the value of the newrenlnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNEWRENLNM(String value) {
        this.newrenlnm = value;
    }

    /**
     * Gets the value of the curstscd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCURSTSCD() {
        return curstscd;
    }

    /**
     * Sets the value of the curstscd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCURSTSCD(String value) {
        this.curstscd = value;
    }

    /**
     * Gets the value of the curstsnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCURSTSNM() {
        return curstsnm;
    }

    /**
     * Sets the value of the curstsnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCURSTSNM(String value) {
        this.curstsnm = value;
    }

    /**
     * Gets the value of the currsncd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCURRSNCD() {
        return currsncd;
    }

    /**
     * Sets the value of the currsncd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCURRSNCD(String value) {
        this.currsncd = value;
    }

    /**
     * Gets the value of the currsnnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCURRSNNM() {
        return currsnnm;
    }

    /**
     * Sets the value of the currsnnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCURRSNNM(String value) {
        this.currsnnm = value;
    }

    /**
     * Gets the value of the curststistmp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCURSTSTISTMP() {
        return curststistmp;
    }

    /**
     * Sets the value of the curststistmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCURSTSTISTMP(XMLGregorianCalendar value) {
        this.curststistmp = value;
    }

}
