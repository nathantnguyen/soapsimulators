
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GetPtyResult" type="{http://workstation.znawebservices.zurichna.com}PtyResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPtyResult"
})
@XmlRootElement(name = "GetPtyResponse")
public class GetPtyResponse {

    @XmlElement(name = "GetPtyResult")
    protected PtyResponse getPtyResult;

    /**
     * Gets the value of the getPtyResult property.
     * 
     * @return
     *     possible object is
     *     {@link PtyResponse }
     *     
     */
    public PtyResponse getGetPtyResult() {
        return getPtyResult;
    }

    /**
     * Sets the value of the getPtyResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link PtyResponse }
     *     
     */
    public void setGetPtyResult(PtyResponse value) {
        this.getPtyResult = value;
    }

}
