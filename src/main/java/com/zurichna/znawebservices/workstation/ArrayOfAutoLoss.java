
package com.zurichna.znawebservices.workstation;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfAutoLoss complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfAutoLoss"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AutoLoss" type="{http://workstation.znawebservices.zurichna.com}AutoLoss" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfAutoLoss", propOrder = {
    "autoLoss"
})
public class ArrayOfAutoLoss {

    @XmlElement(name = "AutoLoss", nillable = true)
    protected List<AutoLoss> autoLoss;

    /**
     * Gets the value of the autoLoss property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the autoLoss property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAutoLoss().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AutoLoss }
     * 
     * 
     */
    public List<AutoLoss> getAutoLoss() {
        if (autoLoss == null) {
            autoLoss = new ArrayList<AutoLoss>();
        }
        return this.autoLoss;
    }

}
