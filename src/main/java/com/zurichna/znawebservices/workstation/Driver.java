
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Driver complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Driver"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DRVR_ID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="DRVR_LIC_FST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DRVR_LIC_LST_NM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MVR_STS_TXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DRVR_STS_OVRID_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DRVR_LIC_GNDR_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DRVR_LIC_BRTH_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="DRVR_LIC_ST_ABBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AUTO_DRVR_LST_ENT_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="MVR_ORDR_DT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="RNDM_SELCT_CD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DRVR_LIC_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MVR_DRVR_SCOR" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="DRVR_OVRID_VIOL_PNTS_CNT" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Driver", propOrder = {
    "drvrid",
    "drvrlicfstnm",
    "drvrliclstnm",
    "mvrststxt",
    "drvrstsovridcd",
    "drvrlicgndrcd",
    "drvrlicbrthdt",
    "drvrlicstabbr",
    "autodrvrlstentdt",
    "mvrordrdt",
    "rndmselctcd",
    "drvrlicnbr",
    "mvrdrvrscor",
    "drvrovridviolpntscnt"
})
public class Driver {

    @XmlElement(name = "DRVR_ID")
    protected int drvrid;
    @XmlElement(name = "DRVR_LIC_FST_NM")
    protected String drvrlicfstnm;
    @XmlElement(name = "DRVR_LIC_LST_NM")
    protected String drvrliclstnm;
    @XmlElement(name = "MVR_STS_TXT")
    protected String mvrststxt;
    @XmlElement(name = "DRVR_STS_OVRID_CD")
    protected String drvrstsovridcd;
    @XmlElement(name = "DRVR_LIC_GNDR_CD")
    protected String drvrlicgndrcd;
    @XmlElement(name = "DRVR_LIC_BRTH_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar drvrlicbrthdt;
    @XmlElement(name = "DRVR_LIC_ST_ABBR")
    protected String drvrlicstabbr;
    @XmlElement(name = "AUTO_DRVR_LST_ENT_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar autodrvrlstentdt;
    @XmlElement(name = "MVR_ORDR_DT", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar mvrordrdt;
    @XmlElement(name = "RNDM_SELCT_CD")
    protected String rndmselctcd;
    @XmlElement(name = "DRVR_LIC_NBR")
    protected String drvrlicnbr;
    @XmlElement(name = "MVR_DRVR_SCOR")
    protected int mvrdrvrscor;
    @XmlElement(name = "DRVR_OVRID_VIOL_PNTS_CNT")
    protected int drvrovridviolpntscnt;

    /**
     * Gets the value of the drvrid property.
     * 
     */
    public int getDRVRID() {
        return drvrid;
    }

    /**
     * Sets the value of the drvrid property.
     * 
     */
    public void setDRVRID(int value) {
        this.drvrid = value;
    }

    /**
     * Gets the value of the drvrlicfstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDRVRLICFSTNM() {
        return drvrlicfstnm;
    }

    /**
     * Sets the value of the drvrlicfstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDRVRLICFSTNM(String value) {
        this.drvrlicfstnm = value;
    }

    /**
     * Gets the value of the drvrliclstnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDRVRLICLSTNM() {
        return drvrliclstnm;
    }

    /**
     * Sets the value of the drvrliclstnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDRVRLICLSTNM(String value) {
        this.drvrliclstnm = value;
    }

    /**
     * Gets the value of the mvrststxt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMVRSTSTXT() {
        return mvrststxt;
    }

    /**
     * Sets the value of the mvrststxt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMVRSTSTXT(String value) {
        this.mvrststxt = value;
    }

    /**
     * Gets the value of the drvrstsovridcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDRVRSTSOVRIDCD() {
        return drvrstsovridcd;
    }

    /**
     * Sets the value of the drvrstsovridcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDRVRSTSOVRIDCD(String value) {
        this.drvrstsovridcd = value;
    }

    /**
     * Gets the value of the drvrlicgndrcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDRVRLICGNDRCD() {
        return drvrlicgndrcd;
    }

    /**
     * Sets the value of the drvrlicgndrcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDRVRLICGNDRCD(String value) {
        this.drvrlicgndrcd = value;
    }

    /**
     * Gets the value of the drvrlicbrthdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDRVRLICBRTHDT() {
        return drvrlicbrthdt;
    }

    /**
     * Sets the value of the drvrlicbrthdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDRVRLICBRTHDT(XMLGregorianCalendar value) {
        this.drvrlicbrthdt = value;
    }

    /**
     * Gets the value of the drvrlicstabbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDRVRLICSTABBR() {
        return drvrlicstabbr;
    }

    /**
     * Sets the value of the drvrlicstabbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDRVRLICSTABBR(String value) {
        this.drvrlicstabbr = value;
    }

    /**
     * Gets the value of the autodrvrlstentdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAUTODRVRLSTENTDT() {
        return autodrvrlstentdt;
    }

    /**
     * Sets the value of the autodrvrlstentdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAUTODRVRLSTENTDT(XMLGregorianCalendar value) {
        this.autodrvrlstentdt = value;
    }

    /**
     * Gets the value of the mvrordrdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMVRORDRDT() {
        return mvrordrdt;
    }

    /**
     * Sets the value of the mvrordrdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMVRORDRDT(XMLGregorianCalendar value) {
        this.mvrordrdt = value;
    }

    /**
     * Gets the value of the rndmselctcd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRNDMSELCTCD() {
        return rndmselctcd;
    }

    /**
     * Sets the value of the rndmselctcd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRNDMSELCTCD(String value) {
        this.rndmselctcd = value;
    }

    /**
     * Gets the value of the drvrlicnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDRVRLICNBR() {
        return drvrlicnbr;
    }

    /**
     * Sets the value of the drvrlicnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDRVRLICNBR(String value) {
        this.drvrlicnbr = value;
    }

    /**
     * Gets the value of the mvrdrvrscor property.
     * 
     */
    public int getMVRDRVRSCOR() {
        return mvrdrvrscor;
    }

    /**
     * Sets the value of the mvrdrvrscor property.
     * 
     */
    public void setMVRDRVRSCOR(int value) {
        this.mvrdrvrscor = value;
    }

    /**
     * Gets the value of the drvrovridviolpntscnt property.
     * 
     */
    public int getDRVROVRIDVIOLPNTSCNT() {
        return drvrovridviolpntscnt;
    }

    /**
     * Sets the value of the drvrovridviolpntscnt property.
     * 
     */
    public void setDRVROVRIDVIOLPNTSCNT(int value) {
        this.drvrovridviolpntscnt = value;
    }

}
