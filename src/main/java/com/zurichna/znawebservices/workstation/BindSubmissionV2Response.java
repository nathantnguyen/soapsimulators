
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BindSubmissionV2Result" type="{http://workstation.znawebservices.zurichna.com}SubmissionBindV2Response" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "bindSubmissionV2Result"
})
@XmlRootElement(name = "BindSubmissionV2Response")
public class BindSubmissionV2Response {

    @XmlElement(name = "BindSubmissionV2Result")
    protected SubmissionBindV2Response bindSubmissionV2Result;

    /**
     * Gets the value of the bindSubmissionV2Result property.
     * 
     * @return
     *     possible object is
     *     {@link SubmissionBindV2Response }
     *     
     */
    public SubmissionBindV2Response getBindSubmissionV2Result() {
        return bindSubmissionV2Result;
    }

    /**
     * Sets the value of the bindSubmissionV2Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubmissionBindV2Response }
     *     
     */
    public void setBindSubmissionV2Result(SubmissionBindV2Response value) {
        this.bindSubmissionV2Result = value;
    }

}
