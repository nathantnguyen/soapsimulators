
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PolicyRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PolicyRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://workstation.znawebservices.zurichna.com}RequestBase"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="POL_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NO_OF_TERMS" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicyRequest", propOrder = {
    "polnbr",
    "noofterms"
})
public class PolicyRequest
    extends RequestBase
{

    @XmlElement(name = "POL_NBR")
    protected String polnbr;
    @XmlElement(name = "NO_OF_TERMS")
    protected int noofterms;

    /**
     * Gets the value of the polnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOLNBR() {
        return polnbr;
    }

    /**
     * Sets the value of the polnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOLNBR(String value) {
        this.polnbr = value;
    }

    /**
     * Gets the value of the noofterms property.
     * 
     */
    public int getNOOFTERMS() {
        return noofterms;
    }

    /**
     * Sets the value of the noofterms property.
     * 
     */
    public void setNOOFTERMS(int value) {
        this.noofterms = value;
    }

}
