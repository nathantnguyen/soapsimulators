
package com.zurichna.znawebservices.workstation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AutoTechPricRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AutoTechPricRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SAN_NUM" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="TOT_MNL_PREM_AMT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MGMT_CAL_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NON_OWNED_CAL_NBR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="autoLoss" type="{http://workstation.znawebservices.zurichna.com}ArrayOfAutoLoss" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AutoTechPricRequest", propOrder = {
    "sannum",
    "totmnlpremamt",
    "mgmtcalnbr",
    "nonownedcalnbr",
    "autoLoss"
})
public class AutoTechPricRequest {

    @XmlElement(name = "SAN_NUM")
    protected long sannum;
    @XmlElement(name = "TOT_MNL_PREM_AMT")
    protected String totmnlpremamt;
    @XmlElement(name = "MGMT_CAL_NBR")
    protected String mgmtcalnbr;
    @XmlElement(name = "NON_OWNED_CAL_NBR")
    protected String nonownedcalnbr;
    protected ArrayOfAutoLoss autoLoss;

    /**
     * Gets the value of the sannum property.
     * 
     */
    public long getSANNUM() {
        return sannum;
    }

    /**
     * Sets the value of the sannum property.
     * 
     */
    public void setSANNUM(long value) {
        this.sannum = value;
    }

    /**
     * Gets the value of the totmnlpremamt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTOTMNLPREMAMT() {
        return totmnlpremamt;
    }

    /**
     * Sets the value of the totmnlpremamt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTOTMNLPREMAMT(String value) {
        this.totmnlpremamt = value;
    }

    /**
     * Gets the value of the mgmtcalnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMGMTCALNBR() {
        return mgmtcalnbr;
    }

    /**
     * Sets the value of the mgmtcalnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMGMTCALNBR(String value) {
        this.mgmtcalnbr = value;
    }

    /**
     * Gets the value of the nonownedcalnbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNONOWNEDCALNBR() {
        return nonownedcalnbr;
    }

    /**
     * Sets the value of the nonownedcalnbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNONOWNEDCALNBR(String value) {
        this.nonownedcalnbr = value;
    }

    /**
     * Gets the value of the autoLoss property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfAutoLoss }
     *     
     */
    public ArrayOfAutoLoss getAutoLoss() {
        return autoLoss;
    }

    /**
     * Sets the value of the autoLoss property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfAutoLoss }
     *     
     */
    public void setAutoLoss(ArrayOfAutoLoss value) {
        this.autoLoss = value;
    }

}
