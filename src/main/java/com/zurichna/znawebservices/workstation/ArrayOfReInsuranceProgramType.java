
package com.zurichna.znawebservices.workstation;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfReInsuranceProgramType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfReInsuranceProgramType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ReInsuranceProgramType" type="{http://workstation.znawebservices.zurichna.com}ReInsuranceProgramType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfReInsuranceProgramType", propOrder = {
    "reInsuranceProgramType"
})
public class ArrayOfReInsuranceProgramType {

    @XmlElement(name = "ReInsuranceProgramType", nillable = true)
    protected List<ReInsuranceProgramType> reInsuranceProgramType;

    /**
     * Gets the value of the reInsuranceProgramType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reInsuranceProgramType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReInsuranceProgramType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReInsuranceProgramType }
     * 
     * 
     */
    public List<ReInsuranceProgramType> getReInsuranceProgramType() {
        if (reInsuranceProgramType == null) {
            reInsuranceProgramType = new ArrayList<ReInsuranceProgramType>();
        }
        return this.reInsuranceProgramType;
    }

}
